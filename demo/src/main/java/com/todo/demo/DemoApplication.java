package com.todo.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.todo.demo.entity.Todo;
import com.todo.demo.entity.User;
import com.todo.demo.repository.TodoRepository;
import com.todo.demo.repository.UserRepository;

@SpringBootApplication
public class DemoApplication  implements  CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	@Autowired
	private UserRepository userRepository;

	@Autowired 
	private TodoRepository todoRepository;
	@Override
	public void run(String... args) throws Exception{
		User user =  new User();
		user.setId(1L);
		user.setPassword("password");
		user.setUsername("test");
		

		Todo todo = new Todo();
		todo.setId(1L);
		todo.setContent("test task");
		todoRepository.save(todo);	
		user.getTodoList().add(todo);
		userRepository.save(user);
	}
}
