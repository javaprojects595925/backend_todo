package com.todo.demo.controller;

import java.util.NoSuchElementException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.todo.demo.entity.Todo;
import com.todo.demo.entity.User;
import com.todo.demo.repository.TodoRepository;
import com.todo.demo.repository.UserRepository;
import com.todo.demo.request.AddTodoRequest;
import com.todo.demo.request.AddUserRequest;

public class UserController {
    private UserRepository userRepository;
    private TodoRepository todoRepository;
    public UserController(UserRepository userRepository, TodoRepository todoRepository){
        this.userRepository = userRepository;
        this.todoRepository = todoRepository;
    }
    @GetMapping("/{userId}")
    public User getUserById(@PathVariable Long userId){
        return userRepository.findById(userId).orElseThrow(()-> new NoSuchElementException());
    }
    @GetMapping("/{todoId}")
    public User getTodoById(@PathVariable Long userId){
        return userRepository.findById(userId).orElseThrow(()-> new NoSuchElementException());
    }
    @PostMapping
    public User addUser(@Request AddUserRequest userRequest){
        User user = new User();
        user.setUsername(user.getUsername());
        user.setPassword(user.getPassword());
        return userRepository.save(user);
        
    }

    @PostMapping("/{userId}/todos")
    public void addTodo(@PathVariable Long userId,@RequestBody AddTodoRequest todoRequest){
        User user = userRepository.findById(userId).orElseThrow(()-> new NoSuchElementException());
        Todo todo = new Todo();
        todo.setContent(todoRequest.getContent());
        user.getTodoList().add(todo);
        todoRepository.save(todo);
        userRepository.save(user);
    }
    @PostMapping("/todos/{todosId}")
    public void  toggleTodoCompleted(@PathVariable Long todoId){
        Todo todo = todoRepository.findById(todoId).orElseThrow(()-> new NoSuchElementException());
        todo.setCompleted(!todo.getCompleted());
        todoRepository.save(todo);
    }
    
}
